﻿using dotnet_ef_core6.DataContext;
using Microsoft.AspNetCore.Mvc;

namespace dotnet_ef_core6.Controllers
{
    public class AssetController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AssetController (ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            IEnumerable<AssetEntity> assets = _context.AssetEntities.ToList();
            return View(assets);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            AssetEntity asset = _context.AssetEntities.Find(id);
            return View(asset);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("NoAsset, AssetName, AssetType, PurchaseYear")] AssetEntity request)
        {
            // tambah ke entity
            _context.AssetEntities.Add(request);
            // commit ke database
            _context.SaveChanges();
            return Redirect("GetAll");
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var entity = _context.AssetEntities.Find(id);
            return View(entity);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            var entity = _context.AssetEntities.Find(id);
            //if (entity == null)
            //{
            //    return Redirect("GetAll");
            //}
            // buang dari entity
            _context.AssetEntities.Remove(entity);
            // commiting to database
            _context.SaveChanges();
            return View(entity);
        }

        [HttpPost]
        public IActionResult Update([Bind("Id, NoAsset, AssetName, AssetType, PurchaseYear")] AssetEntity request)
        {
            //update ke entity
            _context.AssetEntities.Update(request);
            //add to database
            _context.SaveChanges();
            return Redirect("GetAll");
        }
    }
}
