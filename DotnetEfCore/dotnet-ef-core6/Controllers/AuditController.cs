﻿using dotnet_ef_core6.DataContext;
using Microsoft.AspNetCore.Mvc;

namespace dotnet_ef_core6.Controllers
{
    public class AuditController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AuditController (ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            IEnumerable<AuditEntity> audits = _context.AuditEntities.ToList();
            return View(audits);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            AuditEntity audit = _context.AuditEntities.Find(id);
            return View(audit);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("NoAsset, AssetName, SerialNo, PIC, TypeWindows")] AuditEntity request)
        {
            // tambah ke entity
            _context.AuditEntities.Add(request);
            // commit ke database
            _context.SaveChanges();
            return Redirect("GetAll");
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var entity = _context.AuditEntities.Find(id);
            return View(entity);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            var entity = _context.AuditEntities.Find(id);
            //if (entity == null)
            //{
            //    return Redirect("GetAll");
            //}
            // buang dari entity
            _context.AuditEntities.Remove(entity);
            // commiting to database
            _context.SaveChanges();
            return View(entity);
        }

        [HttpPost]
        public IActionResult Update([Bind("Id, NoAsset, AssetName, SerialNo, PIC, TypeWindows")] AuditEntity request)
        {
            //update ke entity
            _context.AuditEntities.Update(request);
            //add to database
            _context.SaveChanges();
            return Redirect("GetAll");
        }
    }
}
