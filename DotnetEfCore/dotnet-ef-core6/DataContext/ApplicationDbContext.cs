﻿using Microsoft.EntityFrameworkCore;

namespace dotnet_ef_core6.DataContext
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<CategoryEntity> CategoryEntities => Set<CategoryEntity>();
        public DbSet<AssetEntity> AssetEntities => Set<AssetEntity>();
        public DbSet<AuditEntity> AuditEntities => Set<AuditEntity>();
    }
}
