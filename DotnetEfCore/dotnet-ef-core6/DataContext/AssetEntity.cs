﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dotnet_ef_core6.DataContext
{
    [Table("tbl_asset")]
    public class AssetEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("no_asset")]
        public string NoAsset { get; set; }

        [Column("asset_name")]
        public string AssetName { get; set; }

        [Column("asset_type")]
        public string AssetType { get; set; }

        [Column("purchase_year")]
        public int PurchaseYear { get; set; }

    }
}
