﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dotnet_ef_core6.DataContext
{
    [Table("tbl_audit")]
    public class AuditEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("no_asset")]
        public string NoAsset { get; set; }

        [Column("asset_name")]
        public string AssetName { get; set; }

        [Column("serial_no")]
        public string SerialNo { get; set; }

        [Column("pic")]
        public string PIC { get; set; }

        [Column("type_windows")]
        public string TypeWindows { get; set; }

    }
}
